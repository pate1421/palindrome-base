package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class PalindromeTest {

	@Test
	public void testIsPalindromeRegular( ) {
		boolean test =Palindrome.isPalindrome("racecar");
		assertTrue("value provided failed palindrome validation", test);
	}

	@Test
	public void testIsPalindromeException( ) {
		boolean test =Palindrome.isPalindrome("thisfails"); 
		assertFalse("value provided....", test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test =Palindrome.isPalindrome("taco cat");
		assertTrue("value provided failed palindrome validation....", test);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test =Palindrome.isPalindrome("race on car");
		assertFalse("value provided....", test);
	}	
	
	public static void main(String[] args) {}
	
}
